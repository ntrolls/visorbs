from match import template_matching
from templatematch import binkley
import pic, sys, os, sips, match, filecmp, shutil, convert, glob
import util

window_size = 5

def copy_snapshot(filename, source_path, source_name, criterion, pass_count):
	name, ext = os.path.splitext(filename)
	pass_file = "%s/%s_%s_pass%d%s" % (source_path, source_name, criterion, pass_count, ext)
	shutil.copyfile(filename, pass_file)

def delete_range(tokens, i, j):
	deleted = []
	deleted.extend(tokens[:i])
	deleted.extend(tokens[j:])
	return deleted

def write(tokens, temp_file):
	out = open(temp_file, "w")
	out.write("".join(tokens))
	out.close()

def write_slice(tokens, deleted, temp_file):
	out = open(temp_file, "w")
	i = 0
	while i < len(tokens):
		if deleted[i]:
			out.write("\n")
		else:
			out.write(tokens[i])
		i += 1
	out.close()

def print_tokens(tokens):
	print "".join(tokens)

def print_slice(tokens, deleted):
	i = 0
	while i < len(tokens):
		if deleted[i]:
			print 
		else:
			print tokens[i].strip()
		i += 1

def slice(tokens, target_img, **kwargs):
	pass_count = 1
	total = len(tokens)

	compiler = kwargs["compiler"]
	convert = kwargs["convert"]
	temp_file = kwargs["temp_file"]
	build_file= kwargs["build_file"]
	threshold = kwargs["threshold"]
	source_path =kwargs["source_path"]
	source_name=kwargs["source_name"]
	source_ext =kwargs["source_ext"]
	criterion = kwargs["criterion"]
	
	# converter = sips.SIPS()
	# matcher = match.template_matching
	matcher = binkley

	ret, built_file = compiler.compile(build_file)
	print ret, built_file
	converted_file = convert(built_file)
	original_shape = util.get_shape(converted_file)

	print ret, converted_file
	threshold = matcher(converted_file, target_img)
	print "threshold:", threshold
	deleted = [False] * len(tokens)
	while(True):
		print "pass: %d" % (pass_count)
		i = 0
		j = 0
		startedwith = len(tokens)
		bDeleted = False

		#loop that sets the window head
		while(i < len(tokens)):
			bCompiles = False
			bPasses = False
			bForward = True
			#starts with a single line window
			j = i + 1
			maxIndex = min(i + window_size + 1, len(tokens) + 1)
			
			while(j < maxIndex):
				deleted = delete_range(tokens, i, j)
				print "trying:", tokens[i:j]
				write(deleted, temp_file)

				print "\n*** pass:%d, attempting %d - %d" % (pass_count, i, j - 1)

				ret, built_file = compiler.compile(build_file)
				bCompiles = (ret == 0)
				print "build", "passes" if ret==0 else "fails", "exitcode:", ret

				if bCompiles:
					# if os.path.exists("harness.png"):
					# 	os.remove("harness.png")
					converted_file = convert(built_file)
					converted_name, converted_ext = os.path.splitext(converted_file)
					print "conversion", ret, converted_file
					pagenated = converted_name + "-0" + converted_ext
					
					if os.path.exists(pagenated):
						print "clearing pagenated results"
						os.rename(pagenated, converted_file)
					shape = util.get_shape(converted_file)
					# if shape != original_shape:
					# 	print "match fail: shape is different"
					# 	print "original:", original_shape
					# 	print "current:", shape
					# 	bMatch = False
					# else:
					match_score = matcher(converted_file, target_img)
					print "match score:", match_score, " threshold:", threshold
					bMatch = (ret == 0) and (match_score <= threshold)
					
					print "matching", "passes" if bMatch else "fails"
					os.remove(converted_file)
					# for filename in glob.glob('temp*.jpg'):
					# 	os.remove(filename)

					if bMatch:
						print "\n*** pass:%d, DELETE %d - %d" % (pass_count, i, j - 1)
						print "========================================================"
						
						tokens = deleted
						bDeleted = True
						bForward = False
						# i -= 1 # so that, after break, we get the same i with the increment
						break
					else:
						print "\n### pass:%d, NO MATCH %d - %d" % (pass_count, i, j - 1)
						# print "".join(candidate_tokens)
						print "========================================================"
						# deleted[i:j] = [False] * (j - i)
						j += 1
				else:
					print "\n### pass:%d, NO BUILD %d - %d" % (pass_count, i, j - 1)
					# print "".join(candidate_tokens)
					print "========================================================"
					# deleted[i:j] = [False] * (j - i)
					j += 1
			if bForward:
				i += 1	
		# write_slice(tokens, deleted, temp_file)
		
		# preserve pass snapshot
		write(tokens, temp_file)
		copy_snapshot(temp_file, source_path, source_name, criterion, pass_count)
		ret, built_file = compiler.compile(build_file)
		copy_snapshot(built_file, source_path, source_name, criterion, pass_count)

		# something happened during this pass, so we try another pass
		print "comparing..."
		if bDeleted:
			print "something deleted - next pass (%d) starts" % (pass_count + 1)
			print
			# print_slice(tokens, deleted)
			print_tokens(tokens)
			pass_count += 1
		# nothing could be deleted in the current pass, so we terminate
		else:
			print "nothing deleted - pass %d terminating" % (pass_count)
			break
	# write_slice(tokens, deleted, temp_file)
	write(tokens, temp_file)
	# return tokens, pass_count, sum([1 if x else 0 for x in deleted]), len(deleted)
	return tokens, pass_count, total - len(tokens), total

# def slice_backward(tokens, target_img, **kwargs):
# 	pass_count = 1

# 	compiler = kwargs["compiler"]
# 	temp_file = kwargs["temp_file"]
# 	build_file= kwargs["build_file"]
# 	threshold = kwargs["threshold"]
	
# 	converter = convert.convert()
# 	matcher = template_matching

# 	deleted = [False] * len(tokens)
	
# 	while(True):
# 		print "pass: %d" % (pass_count)
# 		i = len(tokens)
# 		j = 0
# 		startedwith = len(tokens)
# 		bDeleted = False

# 		#loop that sets the window head
# 		while(i > 0):
			
# 			if deleted[i - 1]:
# 				i -= 1
# 				continue

# 			bCompiles = False
# 			bPasses = False

# 			#starts with a single line window
# 			j = i - 1
# 			candidate_tokens = []
# 			minIndex = max(i - window_size, 0)
			
# 			while(j >= minIndex and deleted[j] != True):
# 				candidate_tokens = tokens[j:i]
# 				# deleted = delete_range(tokens, j, i)
# 				deleted[j:i] = [True] * (i - j)
# 				write_slice(tokens, deleted, temp_file)
				
# 				# print "deleting:", "".join(candidate_tokens)
# 				# exitcode, out = compiler.compile("sliced.pic")
# 				# print "exitcode", exitcode
# 				# print "".join(out)
				
# 				print "\n*** pass:%d, attempting %d - %d" % (pass_count, j, i - 1)

# 				ret, built_file = compiler.compile(build_file)
# 				bCompile = (ret == 0)
# 				print "build", "passes" if ret==0 else "fails", "exitcode:", ret

# 				ret, converted_file = converter.convert(built_file)
# 				converted_name, converted_ext = os.path.splitext(converted_file)
# 				pagenated = converted_name + "-0" + converted_ext
# 				if os.path.exists(pagenated):
# 					os.rename(pagenated, converted_file)
# 				bMatch = matcher(converted_file, target_img) < threshold if bCompile else False
# 				print "matching", "passes" if bMatch else "fails"
# 				for filename in glob.glob('temp*.jpg'):
# 					os.remove(filename)

# 				if bCompile and bMatch:
# 					print "\n*** pass:%d, can delete %d - %d" % (pass_count, j, i - 1)
# 					print "========================================================"
# 					# tokens = deleted
# 					bDeleted = True
# 					break
# 				else:
# 					print "\n### pass:%d, can NOT delete %d - %d" % (pass_count, j, i - 1)
# 					print "".join(candidate_tokens)
# 					print "========================================================"
# 					deleted[j:i] = [False] * (i - j)
# 					j -= 1
# 			i -= 1
# 		write_slice(tokens, deleted, temp_file)

# 		# something happened during this pass, so we try another pass
# 		print "comparing..."
# 		if bDeleted:
# 			print "something deleted - next pass (%d) starts" % (pass_count + 1)
# 			print
# 			print_slice(tokens, deleted)
# 			pass_count += 1
# 		# nothing could be deleted in the current pass, so we terminate
# 		else:
# 			print "nothing deleted - pass %d terminating" % (pass_count)
# 			break
# 	write_slice(tokens, deleted, temp_file)
# 	return tokens
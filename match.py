import cv2, cv
import numpy as np
import itertools
import sys

def template_matching(image_file, template_file):
	image = cv.LoadImage(image_file)
	template = cv.LoadImage(template_file)

	W, H = cv.GetSize(image)
	w, h = cv.GetSize(template)
	
	width = W - w + 1
	height = H - h + 1

	result = cv.CreateImage((width, height), 32, 1)
	cv.MatchTemplate(image, template, result, cv.CV_TM_SQDIFF_NORMED)
	(min_val, max_val, minloc, maxloc) = cv.MinMaxLoc(result)
	return min_val

def showMatch(image_file, template_file):
	image = cv.LoadImage(image_file)
	template = cv.LoadImage(template_file)

	W, H = cv.GetSize(image)
	w, h = cv.GetSize(template)

	width = W - w + 1
	height = H - h + 1

	result = cv.CreateImage((width, height), 32, 1)
	cv.MatchTemplate(image, template, result, cv.CV_TM_SQDIFF_NORMED)
	(min_val, max_val, minloc, maxloc) = cv.MinMaxLoc(result)
	cv.Rectangle(image, minloc, (minloc[0] + w, minloc[1] + h), cv.Scalar(0, 0, 255, 0), 1, 0, 0)
	print (min_val, max_val, minloc, maxloc)
	cv.ShowImage("image", image)
	cv.WaitKey()

# 

if __name__ == "__main__":
	# match()
	# th = getMaxValForTM(sys.argv[1], sys.argv[2])
	# print th
	# print template_matching(sys.argv[1], sys.argv[2])
	# print templateMatching(sys.argv[1], sys.argv[2], float(sys.argv[3]))
	showMatch(sys.argv[1], sys.argv[2])


# def findKeyPointsORB(img, template, distance=200):
# 	# detector = cv2.FeatureDetector_create("SIFT")
# 	# descriptor = cv2.DescriptorExtractor_create("SIFT")

# 	detector = cv2.FastFeatureDetector(16, True)
# 	detector = cv2.GridAdaptedFeatureDetector(detector)
# 	extractor = cv2.DescriptorExtractor_create('ORB')

# 	skp = detector.detect(img)
# 	skp, sd = descriptor.compute(img, skp)

# 	tkp = detector.detect(template)
# 	tkp, td = descriptor.compute(template, tkp)

# 	flann_params = dict(algorithm=1, trees=4)
# 	flann = cv2.flann_Index(sd, flann_params)
# 	idx, dist = flann.knnSearch(td, 1, params={})
# 	del flann

# 	dist = dist[:,0]/2500.0
# 	dist = dist.reshape(-1,).tolist()
# 	idx = idx.reshape(-1).tolist()
# 	indices = range(len(dist))
# 	indices.sort(key=lambda i: dist[i])
# 	dist = [dist[i] for i in indices]
# 	idx = [idx[i] for i in indices]
# 	skp_final = []
# 	for i, dis in itertools.izip(idx, dist):
# 		if dis < distance:
# 			skp_final.append(skp[i])

# 	flann = cv2.flann_Index(td, flann_params)
# 	idx, dist = flann.knnSearch(sd, 1, params={})
# 	del flann

# 	dist = dist[:,0]/2500.0
# 	dist = dist.reshape(-1,).tolist()
# 	idx = idx.reshape(-1).tolist()
# 	indices = range(len(dist))
# 	indices.sort(key=lambda i: dist[i])
# 	dist = [dist[i] for i in indices]
# 	idx = [idx[i] for i in indices]
# 	tkp_final = []
# 	for i, dis in itertools.izip(idx, dist):
# 		if dis < distance:
# 			tkp_final.append(tkp[i])

# 	return skp_final, tkp_final

# def findKeyPoints(img, template, distance=200):
# 	detector = cv2.FeatureDetector_create("SIFT")
# 	descriptor = cv2.DescriptorExtractor_create("SIFT")

# 	skp = detector.detect(img)
# 	skp, sd = descriptor.compute(img, skp)

# 	tkp = detector.detect(template)
# 	tkp, td = descriptor.compute(template, tkp)

# 	flann_params = dict(algorithm=1, trees=4)
# 	flann = cv2.flann_Index(sd, flann_params)
# 	idx, dist = flann.knnSearch(td, 1, params={})
# 	del flann

# 	dist = dist[:,0]/2500.0
# 	dist = dist.reshape(-1,).tolist()
# 	idx = idx.reshape(-1).tolist()
# 	indices = range(len(dist))
# 	indices.sort(key=lambda i: dist[i])
# 	dist = [dist[i] for i in indices]
# 	idx = [idx[i] for i in indices]
# 	skp_final = []
# 	for i, dis in itertools.izip(idx, dist):
# 		if dis < distance:
# 			skp_final.append(skp[i])

# 	flann = cv2.flann_Index(td, flann_params)
# 	idx, dist = flann.knnSearch(sd, 1, params={})
# 	del flann

# 	dist = dist[:,0]/2500.0
# 	dist = dist.reshape(-1,).tolist()
# 	idx = idx.reshape(-1).tolist()
# 	indices = range(len(dist))
# 	indices.sort(key=lambda i: dist[i])
# 	dist = [dist[i] for i in indices]
# 	idx = [idx[i] for i in indices]
# 	tkp_final = []
# 	for i, dis in itertools.izip(idx, dist):
# 		if dis < distance:
# 			tkp_final.append(tkp[i])

# 	return skp_final, tkp_final

# def drawKeyPoints(img, template, skp, tkp, num=-1):
# 	h1, w1 = img.shape[:2]
# 	h2, w2 = template.shape[:2]
# 	nWidth = w1+w2
# 	nHeight = max(h1, h2)
# 	hdif = (h1-h2) / 2
# 	newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
# 	newimg[hdif:hdif + h2, :w2] = template
# 	newimg[:h1, w2:w1+w2] = img

# 	maxlen = min(len(skp), len(tkp))
# 	if num < 0 or num > maxlen:
# 		num = maxlen
# 	for i in range(maxlen):
# 		pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1]+hdif))
# 		pt_b = (int(skp[i].pt[0]+w2), int(skp[i].pt[1]))
# 		cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
# 	return newimg

# def doesMatch(image, target):
# 	img = cv2.imread(image)
# 	temp = cv2.imread(target)
# 	try:
# 		skp, tkp = findKeyPoints(img, temp, 10)
# 	except cv2.error:
# 		return False
# 	print len(skp), len(tkp)
# 	return len(tkp) > 0

# def match():
# 	img = cv2.imread(sys.argv[1])
# 	temp = cv2.imread(sys.argv[2])
# 	try:
# 		dist = int(sys.argv[3])
# 	except IndexError:
# 		dist = 200
# 	try:
# 		num = int(sys.argv[4])
# 	except IndexError:
# 		num = -1
# 	skp, tkp = findKeyPoints(img, temp, dist)
# 	newimg = drawKeyPoints(img, temp, skp, tkp, num)
# 	cv2.imshow("image", newimg)
# 	cv2.waitKey(0)
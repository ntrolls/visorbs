import pdflatex
import sys
import os
import match
import shutil
import convert
import orbs
import sips
import glob
import util

if __name__ == "__main__":
	compiler = pdflatex.PDFLatex()

	source = sys.argv[1]
	source_path, source_name = os.path.split(source)
	source_name, source_ext = os.path.splitext(source_name)

	util.clean(source_name, sys.argv[2])
	target_img = util.prepare_target(source, sys.argv[2]) # use pre-converted-cropped bitmap 

	threshold = float(sys.argv[3]) if len(sys.argv) > 3 else 0.001
	
	criterion_name = os.path.split(target_img)[1]
	criterion_name = os.path.splitext(criterion_name)[0]


	# move source to the experiment directory
	temp_file = "temp_" + source_name + "_" + criterion_name + ".tex"
	
	print "Using temp_file ", temp_file
	shutil.copyfile(source, temp_file)
	print "Copy successful"

	# create harness for TikZ
	harness_file = "harness_" + source_name + "_" + criterion_name + ".tex"
	# shutil.copyfile("harness.tex", harness_file)
	util.prepare_harness(harness_file, temp_file)
	
	# slice!
	ret, temp_pdf = compiler.compile(harness_file)
	print "temp_pdf:", temp_pdf
	
	temp_png = util.convert(temp_pdf)
	# print temp_png
	# ret, temp_png = sips.SIPS().convert(temp_pdf)
	print "temp_png:", temp_png
	print "source:", source
	os.rename(temp_pdf, source.replace(".tex", "_original.pdf"))
	
	lines = open(source, "r").readlines()
	
	tokens, pass_count, deleted, total = orbs.slice(lines, target_img, threshold=threshold, compiler=compiler, 
		build_file=harness_file, temp_file=temp_file, 
		source_path = source_path, source_name=source_name, 
		source_ext = source_ext, criterion=criterion_name, convert=util.convert)

	# produce the result	
	print "end"
	ret, temp_pdf = compiler.compile(harness_file)
	
	os.rename(temp_pdf, source.replace(".tex", "_" + criterion_name + "_sliced.pdf"))
	os.rename(temp_file, source.replace(".tex", "_" + criterion_name + "_sliced.tex"))

	info_file = source.replace(".tex", "_" + criterion_name + "_stat.txt")
	info = open(info_file, "w")
	info.write("pass,deleted_lines,original_lines\n")
	info.write("%d,%d,%d\n" % (pass_count, deleted, total))
	info.close()

	for filename in glob.glob("harness_" + source_name + "_" + criterion_name + ".*"):
		os.remove(filename)



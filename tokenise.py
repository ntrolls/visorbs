import antlr3
import LatexLexer
import sys

def getTokens(filename):
	char_stream = antlr3.ANTLRInputStream(open(sys.argv[1], "r"))
	lexer = LatexLexer.LatexLexer(char_stream)
	tokenstream = antlr3.CommonTokenStream(lexer)
	return tokenstream.getTokens()
import pic, sys, match, convert, orbs, shutil, os, sips, glob

if __name__ == "__main__":
	source = sys.argv[1]
	target_pdf = sys.argv[2]
	ret, target_img = sips.SIPS().convert(target_pdf)

	threshold = float(sys.argv[3]) if len(sys.argv) > 3 else 0.001
	
	source_path, source_name = os.path.split(source)
	source_name, source_ext = os.path.splitext(source_name)

	criterion_name = os.path.split(target_img)[1]
	criterion_name = os.path.splitext(criterion_name)[0]

	# .pic specific setting
	temp_file = "temp_" + source_name + "_" + criterion_name + ".pic"
	compiler = pic.Pic()
	print "Using temp_file ", temp_file
	shutil.copyfile(source, temp_file)
	print "Copy successful"
	
	# slice!
	ret, temp_pdf = compiler.compile(temp_file)
	print "temp_pdf:", temp_pdf

	ret, temp_png = convert.convert().convert(temp_pdf)
	# print temp_png
	# ret, temp_png = sips.SIPS().convert(temp_pdf)
	print "temp_png:", temp_png
	print "source:", source
	os.rename(temp_pdf, source.replace(".pic", "_original.pdf"))

	lines = open(source, "r").readlines()
	
	tokens, pass_count, deleted, total = orbs.slice(lines, target_img, threshold=threshold, compiler=compiler, 
		build_file=temp_file, temp_file=temp_file, 
		source_path = source_path, source_name=source_name, 
		source_ext = source_ext, criterion=criterion_name)

	# produce the result	
	print "end"
	ret, temp_pdf = compiler.compile(temp_file)

	os.rename(temp_pdf, source.replace(".pic", "_" + criterion_name + "_sliced.pdf"))
	os.rename(temp_file, source.replace(".pic", "_" + criterion_name + "_sliced.tex"))

	info_file = source.replace(".pic", "_" + criterion_name + "_stat.txt")
	info = open(info_file, "w")
	info.write("pass,deleted_lines,original_lines\n")
	info.write("%d,%d,%d\n" % (pass_count, deleted, total))
	info.close()

	for filename in glob.glob("temp_" + source_name + "_" + criterion_name + ".*"):
		os.remove(filename)
	os.remove(target_img)
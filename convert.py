import subprocess, tempfile, time, threading, sys, os, signal

class convert(object):
	def __init__(self):
		self.cmd = "convert -density 150 --units pixelsperinch"
		self.output_name = ""
		self.working_dir = None
		self.process = None
		self.out = None
		self.err = None
		self.exitcode = None
	
	def convert(self, inp):
		def target():
			input_name, input_ext = os.path.splitext(inp)
			self.output_name = input_name + ".png"
			# print inp, "->", self.output_name
			self.process = subprocess.Popen(self.cmd + " " + inp + " " + self.output_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
			self.out, self.error = self.process.communicate()
			self.exitcode = self.process.returncode
		thread = threading.Thread(target=target)
		thread.start()
		thread.join(10)
		if thread.is_alive():
			os.killpg(self.process.pid, signal.SIGTERM)
			self.process.kill()
			return -9, None
		return self.exitcode, self.output_name

def convert(inputfile):
	input_name, input_ext = os.path.splitext(inputfile)
	output_name = input_name + ".png"
	cmd = "convert -density 150 -units pixelsperinch"
	process = subprocess.Popen(cmd + " " + inputfile + " " + output_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
	out, error = process.communicate()
	exitcode = process.returncode
	return exitcode, output_name

def crop(inputfile, x, y, w, h, output_name):
	path, input_filename = os.path.split(inputfile)
	input_name, input_ext = os.path.splitext(inputfile)

	output_name = os.path.join(path, output_name + ".png")
	cmd = "convert -crop %dx%d+%d+%d" % (w, h, x, y)
	process = subprocess.Popen(cmd + " " + inputfile + " " + output_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
	out, error = process.communicate()
	exitcode = process.returncode
	print out
	print error
	return exitcode, output_name	

if __name__ == "__main__":
	crop(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), sys.argv[6])
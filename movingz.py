import os
import pdflatex
import sips
import match

contents = """
\\documentclass{standalone}
\\usepackage{pagecolor}
\\pagecolor{white}

\\usepackage{tikz}
\\usetikzlibrary{calc,decorations.markings,decorations.pathreplacing,calc,arrows,shapes,decorations}
\\usepackage{verbatim}

\\newcommand{\\drawarcdelta}[4]{
  \\draw ($#1+(#2:#4)$) arc[start angle=#2, delta angle=#3, radius=#4];
}

\\newcommand{\\drawlabeledarcdelta}[6]{
  \\drawarcdelta{#1}{#2}{#3}{#4}
  \\node at ($#1+(#2+#3/2:#6)$) {#5};
}

\\begin{document}

\\begin{tikzpicture}[join=round]
    \\draw[arrows=-,line width=.4pt](0,0)--(%.03f,-.1);
        (-2.1,-.153) node[left] {$z$};
    \\begin{scope}[xshift=3.5cm]
    \\filldraw[fill=red,fill opacity=0.5](-.516,1.45)--(-.775,-.424)--(.274,-.5)
                                         --(.183,1.4)--cycle;
    \\draw (-.209,.482)+(-60:.25) [yscale=1.3,->] arc(-60:240:.25);
    \\fill[black,font=\\footnotesize]
                    (.183,1.4) node [above] {$P_{01}$}
                    (.274,-.5) node [below] {$P_{11}$};
    \\end{scope}
\\end{tikzpicture}


\\end{document}
"""

compiler = pdflatex.PDFLatex()
converter = sips.SIPS()
matcher = match.template_matching

res = []

for x in range(100):
	p = -2.0 + float(x) / float(1000)	
	f = open("mz.tex", "w")
	f.write(contents % (p))
	f.flush()
	f.close()

	compiler.compile("mz.tex")
	converter.convert("mz.pdf")
	score = matcher("mz.png", "subjects/cone/target5.png")

	res.append((p, score))

	os.remove("mz.pdf")
	os.remove("mz.tex")

for pair in res:
	print "%f,%f" % (pair[0], pair[1])



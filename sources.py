cone = {
	"target1": (410, 146, 221, 216),
	"target2": (132, 101, 277, 264),
	"target3": (282, 100, 25, 24),
	"target4": (146, 290, 45, 25),
	"target5": (471, 235, 39, 50),
	}

hydrogen = {
	"target1": (117, 333, 277, 328),
	"target2": (262, 167, 764, 57),
	"target3": (387, 379, 167, 247),
	"target4": (765, 361, 303, 163),
	"target5": (878, 217, 152, 116),
	}

raindrop = {
	"target1": (364, 135, 77, 110),
	"target2": (199, 206, 107, 47),
	"target3": (536, 321, 97, 65),
	"target4": (507, 466, 102, 125),
	"target5": (396, 320, 46, 143),
	}

shapes = {
	"target1": (366, 34, 90, 90),
	"target2": (194, 126, 286, 129),
	"target3": (201, 293, 129, 27),
	"target4": (53, 490, 412, 32),
	"target5": (583, 452, 82, 36),
	}

threecols = {
	"target1": (0, 150, 800, 190),
	"target2": (210, 350, 360, 380),
	"target3": (21, 136, 157, 37),
	"target4": (214, 231, 223, 42),
	"target5": (7, 218, 185, 2133),
}

subjects = {
	"subjects/cone/cone.tex":cone,
	"subjects/hydrogen/hydrogen.tex":hydrogen,
	"subjects/raindrop/raindrop.tex":raindrop,
	"subjects/shapes/shapes.tex":shapes,
	"subjects/threecols/threecols.html":threecols,
	"subjects/shapes1/shapes_target1_sliced.tex":shapes,
}

def get_cropbox(subject, target):
	return subjects[subject][target]
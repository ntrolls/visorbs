import cv
import math
import sys
from PIL import Image
import numpy as np

def binkley(source_img, template_img, show=False):
	try:
		source = np.asarray(Image.open(source_img).convert("L")).astype(np.int16)
		template = np.asarray(Image.open(template_img).convert("L")).astype(np.int16)
		if source.shape[0] < template.shape[0] or source.shape[1] < template.shape[1]:
			return np.iinfo(np.int16).max
	except IOError:
		return np.iinfo(npint16).max
	return _binkley(source, template, show)[0]

def _binkley(source, template, show=False):
	sy, sx = source.shape # rows, columns
	ty, tx = template.shape # rows, columns

	min_x = -1
	min_y = -1
	min_score = np.iinfo(np.int16).max
	total = (sx - tx + 1) * (sy - ty + 1)
	count = 0

	r = np.array([np.iinfo(np.int32).max] * total, dtype=np.int32).reshape(sy - ty + 1, sx - tx + 1)
	t = 200

	for x in range(sx - tx + 1):
		for y in range(sy - ty + 1):
			r[y][x] = np.iinfo(np.int16).max
			c = (template < t) & (source[y:y + ty, x:x + tx] > t)
			if c.any():
				continue
			else:
				r[y][x] = np.absolute(template - source[y:y + ty, x:x + tx]).sum()
	
	print r
	min_y, min_x = np.unravel_index(np.argmin(r), dims=r.shape)
	min_score = r[min_y][min_x]
	# print (r < 32767).any()

	if(show):
		print min_score, min_y, min_x
		image = cv.fromarray(source.astype(np.uint8))
		cv.Rectangle(image, (min_x, min_y), (min_x + tx, min_y + ty), cv.Scalar(0, 0, 255, 0), 1, 0, 0)
		# image = cv.fromarray((template - source[min_y:min_y+ty,min_x:min_x+tx]).astype(np.uint8))
		cv.ShowImage("image", image)
		cv.WaitKey()

	return min_score, min_y, min_x
		
if __name__ == '__main__':
	print binkley(sys.argv[1], sys.argv[2], True)
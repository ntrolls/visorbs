import sys
import cv
import numpy as np
from PIL import Image

def diff(source_img, template_img):
	source = np.asarray(Image.open(source_img).convert("L")).astype(np.int16)
	template = np.asarray(Image.open(template_img).convert("L")).astype(np.int16)
	
	if source.shape != template.shape:
		print "not the same size"
		return

	d = source - template
	print d[np.nonzero(d)]
	print d.astype(np.uint8)[np.nonzero(d)]
	image = cv.fromarray(d.astype(np.uint8))
	cv.ShowImage("image", image)
	cv.WaitKey()
	
if __name__ == '__main__':
	diff(sys.argv[1], sys.argv[2])
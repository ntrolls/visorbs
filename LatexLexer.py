# $ANTLR 3.1.3 Mar 18, 2009 10:09:25 LatexLexer.g 2013-04-17 13:23:37

import sys
from antlr3 import *
from antlr3.compat import set, frozenset


# for convenience in actions
HIDDEN = BaseRecognizer.HIDDEN

# token types
INTEGER=31
LT=19
RIGHTS=26
SHARP=14
RIGHTP=28
LINE_COMMENT=7
ALPHABET=5
FLOAT=32
SEMICOLON=17
RIGHTC=24
MINUS=11
AND=8
LEFTP=27
EOF=-1
MATH=29
LEFTS=25
COMMAND=6
COLON=16
WS=34
NEWLINE=30
MULTIPLY=12
COMMA=15
LEFTC=23
SQ=22
FILTER=21
ASSIGN=9
GT=20
PLUS=10
DIGIT=4
DOT=18
DIVIDE=13
STRING=33


class LatexLexer(Lexer):

    grammarFileName = "LatexLexer.g"
    antlr_version = version_str_to_tuple("3.1.3 Mar 18, 2009 10:09:25")
    antlr_version_str = "3.1.3 Mar 18, 2009 10:09:25"

    def __init__(self, input=None, state=None):
        if state is None:
            state = RecognizerSharedState()
        super(LatexLexer, self).__init__(input, state)


        self.dfa11 = self.DFA11(
            self, 11,
            eot = self.DFA11_eot,
            eof = self.DFA11_eof,
            min = self.DFA11_min,
            max = self.DFA11_max,
            accept = self.DFA11_accept,
            special = self.DFA11_special,
            transition = self.DFA11_transition
            )






    # $ANTLR start "DIGIT"
    def mDIGIT(self, ):

        try:
            # LatexLexer.g:8:15: ( ( '0' .. '9' ) )
            # LatexLexer.g:8:17: ( '0' .. '9' )
            pass 
            # LatexLexer.g:8:17: ( '0' .. '9' )
            # LatexLexer.g:8:18: '0' .. '9'
            pass 
            self.matchRange(48, 57)







        finally:

            pass

    # $ANTLR end "DIGIT"



    # $ANTLR start "ALPHABET"
    def mALPHABET(self, ):

        try:
            _type = ALPHABET
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:10:9: ( ( 'A' .. 'Z' | 'a' .. 'z' ) )
            # LatexLexer.g:10:11: ( 'A' .. 'Z' | 'a' .. 'z' )
            pass 
            if (65 <= self.input.LA(1) <= 90) or (97 <= self.input.LA(1) <= 122):
                self.input.consume()
            else:
                mse = MismatchedSetException(None, self.input)
                self.recover(mse)
                raise mse




            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "ALPHABET"



    # $ANTLR start "COMMAND"
    def mCOMMAND(self, ):

        try:
            _type = COMMAND
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:12:8: ( '\\\\' ( ALPHABET )+ )
            # LatexLexer.g:12:10: '\\\\' ( ALPHABET )+
            pass 
            self.match(92)
            # LatexLexer.g:12:14: ( ALPHABET )+
            cnt1 = 0
            while True: #loop1
                alt1 = 2
                LA1_0 = self.input.LA(1)

                if ((65 <= LA1_0 <= 90) or (97 <= LA1_0 <= 122)) :
                    alt1 = 1


                if alt1 == 1:
                    # LatexLexer.g:12:15: ALPHABET
                    pass 
                    self.mALPHABET()


                else:
                    if cnt1 >= 1:
                        break #loop1

                    eee = EarlyExitException(1, self.input)
                    raise eee

                cnt1 += 1



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "COMMAND"



    # $ANTLR start "LINE_COMMENT"
    def mLINE_COMMENT(self, ):

        try:
            _type = LINE_COMMENT
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:13:13: ( '\\%' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
            # LatexLexer.g:13:15: '\\%' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
            pass 
            self.match(37)
            # LatexLexer.g:13:20: (~ ( '\\n' | '\\r' ) )*
            while True: #loop2
                alt2 = 2
                LA2_0 = self.input.LA(1)

                if ((0 <= LA2_0 <= 9) or (11 <= LA2_0 <= 12) or (14 <= LA2_0 <= 65535)) :
                    alt2 = 1


                if alt2 == 1:
                    # LatexLexer.g:13:20: ~ ( '\\n' | '\\r' )
                    pass 
                    if (0 <= self.input.LA(1) <= 9) or (11 <= self.input.LA(1) <= 12) or (14 <= self.input.LA(1) <= 65535):
                        self.input.consume()
                    else:
                        mse = MismatchedSetException(None, self.input)
                        self.recover(mse)
                        raise mse



                else:
                    break #loop2
            # LatexLexer.g:13:34: ( '\\r' )?
            alt3 = 2
            LA3_0 = self.input.LA(1)

            if (LA3_0 == 13) :
                alt3 = 1
            if alt3 == 1:
                # LatexLexer.g:13:34: '\\r'
                pass 
                self.match(13)



            self.match(10)
            #action start
            _channel=HIDDEN;
            #action end



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "LINE_COMMENT"



    # $ANTLR start "AND"
    def mAND(self, ):

        try:
            _type = AND
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:15:4: ( '&' )
            # LatexLexer.g:15:6: '&'
            pass 
            self.match(38)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "AND"



    # $ANTLR start "ASSIGN"
    def mASSIGN(self, ):

        try:
            _type = ASSIGN
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:16:7: ( '=' )
            # LatexLexer.g:16:9: '='
            pass 
            self.match(61)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "ASSIGN"



    # $ANTLR start "PLUS"
    def mPLUS(self, ):

        try:
            _type = PLUS
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:17:5: ( '+' )
            # LatexLexer.g:17:7: '+'
            pass 
            self.match(43)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "PLUS"



    # $ANTLR start "MINUS"
    def mMINUS(self, ):

        try:
            _type = MINUS
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:18:6: ( '-' )
            # LatexLexer.g:18:8: '-'
            pass 
            self.match(45)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "MINUS"



    # $ANTLR start "MULTIPLY"
    def mMULTIPLY(self, ):

        try:
            _type = MULTIPLY
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:19:9: ( '*' )
            # LatexLexer.g:19:11: '*'
            pass 
            self.match(42)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "MULTIPLY"



    # $ANTLR start "DIVIDE"
    def mDIVIDE(self, ):

        try:
            _type = DIVIDE
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:20:7: ( '/' )
            # LatexLexer.g:20:9: '/'
            pass 
            self.match(47)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "DIVIDE"



    # $ANTLR start "SHARP"
    def mSHARP(self, ):

        try:
            _type = SHARP
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:22:6: ( '#' )
            # LatexLexer.g:22:8: '#'
            pass 
            self.match(35)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "SHARP"



    # $ANTLR start "COMMA"
    def mCOMMA(self, ):

        try:
            _type = COMMA
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:23:6: ( ',' )
            # LatexLexer.g:23:8: ','
            pass 
            self.match(44)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "COMMA"



    # $ANTLR start "COLON"
    def mCOLON(self, ):

        try:
            _type = COLON
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:24:6: ( ':' )
            # LatexLexer.g:24:8: ':'
            pass 
            self.match(58)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "COLON"



    # $ANTLR start "SEMICOLON"
    def mSEMICOLON(self, ):

        try:
            _type = SEMICOLON
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:25:10: ( ';' )
            # LatexLexer.g:25:12: ';'
            pass 
            self.match(59)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "SEMICOLON"



    # $ANTLR start "DOT"
    def mDOT(self, ):

        try:
            _type = DOT
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:26:4: ( '.' )
            # LatexLexer.g:26:6: '.'
            pass 
            self.match(46)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "DOT"



    # $ANTLR start "LT"
    def mLT(self, ):

        try:
            _type = LT
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:27:3: ( '<' )
            # LatexLexer.g:27:5: '<'
            pass 
            self.match(60)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "LT"



    # $ANTLR start "GT"
    def mGT(self, ):

        try:
            _type = GT
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:28:3: ( '>' )
            # LatexLexer.g:28:5: '>'
            pass 
            self.match(62)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "GT"



    # $ANTLR start "FILTER"
    def mFILTER(self, ):

        try:
            _type = FILTER
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:29:7: ( '|' )
            # LatexLexer.g:29:9: '|'
            pass 
            self.match(124)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "FILTER"



    # $ANTLR start "SQ"
    def mSQ(self, ):

        try:
            _type = SQ
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:30:3: ( '\\'' )
            # LatexLexer.g:30:5: '\\''
            pass 
            self.match(39)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "SQ"



    # $ANTLR start "LEFTC"
    def mLEFTC(self, ):

        try:
            _type = LEFTC
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:32:6: ( '{' )
            # LatexLexer.g:32:8: '{'
            pass 
            self.match(123)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "LEFTC"



    # $ANTLR start "RIGHTC"
    def mRIGHTC(self, ):

        try:
            _type = RIGHTC
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:33:7: ( '}' )
            # LatexLexer.g:33:9: '}'
            pass 
            self.match(125)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "RIGHTC"



    # $ANTLR start "LEFTS"
    def mLEFTS(self, ):

        try:
            _type = LEFTS
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:35:6: ( '[' )
            # LatexLexer.g:35:8: '['
            pass 
            self.match(91)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "LEFTS"



    # $ANTLR start "RIGHTS"
    def mRIGHTS(self, ):

        try:
            _type = RIGHTS
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:36:7: ( ']' )
            # LatexLexer.g:36:9: ']'
            pass 
            self.match(93)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "RIGHTS"



    # $ANTLR start "LEFTP"
    def mLEFTP(self, ):

        try:
            _type = LEFTP
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:38:6: ( '(' )
            # LatexLexer.g:38:8: '('
            pass 
            self.match(40)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "LEFTP"



    # $ANTLR start "RIGHTP"
    def mRIGHTP(self, ):

        try:
            _type = RIGHTP
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:39:7: ( ')' )
            # LatexLexer.g:39:9: ')'
            pass 
            self.match(41)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "RIGHTP"



    # $ANTLR start "MATH"
    def mMATH(self, ):

        try:
            _type = MATH
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:41:5: ( '$' )
            # LatexLexer.g:41:7: '$'
            pass 
            self.match(36)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "MATH"



    # $ANTLR start "NEWLINE"
    def mNEWLINE(self, ):

        try:
            _type = NEWLINE
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:43:8: ( '\\n' )
            # LatexLexer.g:43:10: '\\n'
            pass 
            self.match(10)



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "NEWLINE"



    # $ANTLR start "INTEGER"
    def mINTEGER(self, ):

        try:
            _type = INTEGER
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:45:8: ( ( '-' )? ( DIGIT )+ )
            # LatexLexer.g:45:10: ( '-' )? ( DIGIT )+
            pass 
            # LatexLexer.g:45:10: ( '-' )?
            alt4 = 2
            LA4_0 = self.input.LA(1)

            if (LA4_0 == 45) :
                alt4 = 1
            if alt4 == 1:
                # LatexLexer.g:45:11: '-'
                pass 
                self.match(45)



            # LatexLexer.g:45:17: ( DIGIT )+
            cnt5 = 0
            while True: #loop5
                alt5 = 2
                LA5_0 = self.input.LA(1)

                if ((48 <= LA5_0 <= 57)) :
                    alt5 = 1


                if alt5 == 1:
                    # LatexLexer.g:45:17: DIGIT
                    pass 
                    self.mDIGIT()


                else:
                    if cnt5 >= 1:
                        break #loop5

                    eee = EarlyExitException(5, self.input)
                    raise eee

                cnt5 += 1



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "INTEGER"



    # $ANTLR start "FLOAT"
    def mFLOAT(self, ):

        try:
            _type = FLOAT
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:46:6: ( ( '-' )? ( DIGIT )+ '.' ( DIGIT )+ )
            # LatexLexer.g:46:8: ( '-' )? ( DIGIT )+ '.' ( DIGIT )+
            pass 
            # LatexLexer.g:46:8: ( '-' )?
            alt6 = 2
            LA6_0 = self.input.LA(1)

            if (LA6_0 == 45) :
                alt6 = 1
            if alt6 == 1:
                # LatexLexer.g:46:9: '-'
                pass 
                self.match(45)



            # LatexLexer.g:46:14: ( DIGIT )+
            cnt7 = 0
            while True: #loop7
                alt7 = 2
                LA7_0 = self.input.LA(1)

                if ((48 <= LA7_0 <= 57)) :
                    alt7 = 1


                if alt7 == 1:
                    # LatexLexer.g:46:15: DIGIT
                    pass 
                    self.mDIGIT()


                else:
                    if cnt7 >= 1:
                        break #loop7

                    eee = EarlyExitException(7, self.input)
                    raise eee

                cnt7 += 1
            self.match(46)
            # LatexLexer.g:46:27: ( DIGIT )+
            cnt8 = 0
            while True: #loop8
                alt8 = 2
                LA8_0 = self.input.LA(1)

                if ((48 <= LA8_0 <= 57)) :
                    alt8 = 1


                if alt8 == 1:
                    # LatexLexer.g:46:28: DIGIT
                    pass 
                    self.mDIGIT()


                else:
                    if cnt8 >= 1:
                        break #loop8

                    eee = EarlyExitException(8, self.input)
                    raise eee

                cnt8 += 1



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "FLOAT"



    # $ANTLR start "STRING"
    def mSTRING(self, ):

        try:
            _type = STRING
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:48:7: ( ( ALPHABET | '!' | '?' | '.' | MINUS )+ )
            # LatexLexer.g:48:9: ( ALPHABET | '!' | '?' | '.' | MINUS )+
            pass 
            # LatexLexer.g:48:9: ( ALPHABET | '!' | '?' | '.' | MINUS )+
            cnt9 = 0
            while True: #loop9
                alt9 = 2
                LA9_0 = self.input.LA(1)

                if (LA9_0 == 33 or (45 <= LA9_0 <= 46) or LA9_0 == 63 or (65 <= LA9_0 <= 90) or (97 <= LA9_0 <= 122)) :
                    alt9 = 1


                if alt9 == 1:
                    # LatexLexer.g:
                    pass 
                    if self.input.LA(1) == 33 or (45 <= self.input.LA(1) <= 46) or self.input.LA(1) == 63 or (65 <= self.input.LA(1) <= 90) or (97 <= self.input.LA(1) <= 122):
                        self.input.consume()
                    else:
                        mse = MismatchedSetException(None, self.input)
                        self.recover(mse)
                        raise mse



                else:
                    if cnt9 >= 1:
                        break #loop9

                    eee = EarlyExitException(9, self.input)
                    raise eee

                cnt9 += 1



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "STRING"



    # $ANTLR start "WS"
    def mWS(self, ):

        try:
            _type = WS
            _channel = DEFAULT_CHANNEL

            # LatexLexer.g:49:5: ( ( ' ' | '\\r' | '\\t' )+ )
            # LatexLexer.g:49:7: ( ' ' | '\\r' | '\\t' )+
            pass 
            # LatexLexer.g:49:7: ( ' ' | '\\r' | '\\t' )+
            cnt10 = 0
            while True: #loop10
                alt10 = 2
                LA10_0 = self.input.LA(1)

                if (LA10_0 == 9 or LA10_0 == 13 or LA10_0 == 32) :
                    alt10 = 1


                if alt10 == 1:
                    # LatexLexer.g:
                    pass 
                    if self.input.LA(1) == 9 or self.input.LA(1) == 13 or self.input.LA(1) == 32:
                        self.input.consume()
                    else:
                        mse = MismatchedSetException(None, self.input)
                        self.recover(mse)
                        raise mse



                else:
                    if cnt10 >= 1:
                        break #loop10

                    eee = EarlyExitException(10, self.input)
                    raise eee

                cnt10 += 1
            #action start
            _channel=HIDDEN;
            #action end



            self._state.type = _type
            self._state.channel = _channel

        finally:

            pass

    # $ANTLR end "WS"



    def mTokens(self):
        # LatexLexer.g:1:8: ( ALPHABET | COMMAND | LINE_COMMENT | AND | ASSIGN | PLUS | MINUS | MULTIPLY | DIVIDE | SHARP | COMMA | COLON | SEMICOLON | DOT | LT | GT | FILTER | SQ | LEFTC | RIGHTC | LEFTS | RIGHTS | LEFTP | RIGHTP | MATH | NEWLINE | INTEGER | FLOAT | STRING | WS )
        alt11 = 30
        alt11 = self.dfa11.predict(self.input)
        if alt11 == 1:
            # LatexLexer.g:1:10: ALPHABET
            pass 
            self.mALPHABET()


        elif alt11 == 2:
            # LatexLexer.g:1:19: COMMAND
            pass 
            self.mCOMMAND()


        elif alt11 == 3:
            # LatexLexer.g:1:27: LINE_COMMENT
            pass 
            self.mLINE_COMMENT()


        elif alt11 == 4:
            # LatexLexer.g:1:40: AND
            pass 
            self.mAND()


        elif alt11 == 5:
            # LatexLexer.g:1:44: ASSIGN
            pass 
            self.mASSIGN()


        elif alt11 == 6:
            # LatexLexer.g:1:51: PLUS
            pass 
            self.mPLUS()


        elif alt11 == 7:
            # LatexLexer.g:1:56: MINUS
            pass 
            self.mMINUS()


        elif alt11 == 8:
            # LatexLexer.g:1:62: MULTIPLY
            pass 
            self.mMULTIPLY()


        elif alt11 == 9:
            # LatexLexer.g:1:71: DIVIDE
            pass 
            self.mDIVIDE()


        elif alt11 == 10:
            # LatexLexer.g:1:78: SHARP
            pass 
            self.mSHARP()


        elif alt11 == 11:
            # LatexLexer.g:1:84: COMMA
            pass 
            self.mCOMMA()


        elif alt11 == 12:
            # LatexLexer.g:1:90: COLON
            pass 
            self.mCOLON()


        elif alt11 == 13:
            # LatexLexer.g:1:96: SEMICOLON
            pass 
            self.mSEMICOLON()


        elif alt11 == 14:
            # LatexLexer.g:1:106: DOT
            pass 
            self.mDOT()


        elif alt11 == 15:
            # LatexLexer.g:1:110: LT
            pass 
            self.mLT()


        elif alt11 == 16:
            # LatexLexer.g:1:113: GT
            pass 
            self.mGT()


        elif alt11 == 17:
            # LatexLexer.g:1:116: FILTER
            pass 
            self.mFILTER()


        elif alt11 == 18:
            # LatexLexer.g:1:123: SQ
            pass 
            self.mSQ()


        elif alt11 == 19:
            # LatexLexer.g:1:126: LEFTC
            pass 
            self.mLEFTC()


        elif alt11 == 20:
            # LatexLexer.g:1:132: RIGHTC
            pass 
            self.mRIGHTC()


        elif alt11 == 21:
            # LatexLexer.g:1:139: LEFTS
            pass 
            self.mLEFTS()


        elif alt11 == 22:
            # LatexLexer.g:1:145: RIGHTS
            pass 
            self.mRIGHTS()


        elif alt11 == 23:
            # LatexLexer.g:1:152: LEFTP
            pass 
            self.mLEFTP()


        elif alt11 == 24:
            # LatexLexer.g:1:158: RIGHTP
            pass 
            self.mRIGHTP()


        elif alt11 == 25:
            # LatexLexer.g:1:165: MATH
            pass 
            self.mMATH()


        elif alt11 == 26:
            # LatexLexer.g:1:170: NEWLINE
            pass 
            self.mNEWLINE()


        elif alt11 == 27:
            # LatexLexer.g:1:178: INTEGER
            pass 
            self.mINTEGER()


        elif alt11 == 28:
            # LatexLexer.g:1:186: FLOAT
            pass 
            self.mFLOAT()


        elif alt11 == 29:
            # LatexLexer.g:1:192: STRING
            pass 
            self.mSTRING()


        elif alt11 == 30:
            # LatexLexer.g:1:199: WS
            pass 
            self.mWS()







    # lookup tables for DFA #11

    DFA11_eot = DFA.unpack(
        u"\1\uffff\1\36\5\uffff\1\37\6\uffff\1\40\14\uffff\1\41\7\uffff"
        )

    DFA11_eof = DFA.unpack(
        u"\43\uffff"
        )

    DFA11_min = DFA.unpack(
        u"\1\11\1\41\5\uffff\1\41\6\uffff\1\41\14\uffff\1\56\7\uffff"
        )

    DFA11_max = DFA.unpack(
        u"\1\175\1\172\5\uffff\1\172\6\uffff\1\172\14\uffff\1\71\7\uffff"
        )

    DFA11_accept = DFA.unpack(
        u"\2\uffff\1\2\1\3\1\4\1\5\1\6\1\uffff\1\10\1\11\1\12\1\13\1\14\1"
        u"\15\1\uffff\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27\1\30\1"
        u"\31\1\32\1\uffff\1\35\1\36\1\1\1\7\1\16\1\33\1\34"
        )

    DFA11_special = DFA.unpack(
        u"\43\uffff"
        )

            
    DFA11_transition = [
        DFA.unpack(u"\1\35\1\32\2\uffff\1\35\22\uffff\1\35\1\34\1\uffff\1"
        u"\12\1\31\1\3\1\4\1\22\1\27\1\30\1\10\1\6\1\13\1\7\1\16\1\11\12"
        u"\33\1\14\1\15\1\17\1\5\1\20\1\34\1\uffff\32\1\1\25\1\2\1\26\3\uffff"
        u"\32\1\1\23\1\21\1\24"),
        DFA.unpack(u"\1\34\13\uffff\2\34\20\uffff\1\34\1\uffff\32\34\6\uffff"
        u"\32\34"),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u"\1\34\13\uffff\2\34\1\uffff\12\33\5\uffff\1\34\1\uffff"
        u"\32\34\6\uffff\32\34"),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u"\1\34\13\uffff\2\34\20\uffff\1\34\1\uffff\32\34\6\uffff"
        u"\32\34"),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u"\1\42\1\uffff\12\33"),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u""),
        DFA.unpack(u"")
    ]

    # class definition for DFA #11

    class DFA11(DFA):
        pass


 



def main(argv, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr):
    from antlr3.main import LexerMain
    main = LexerMain(LatexLexer)
    main.stdin = stdin
    main.stdout = stdout
    main.stderr = stderr
    main.execute(argv)


if __name__ == '__main__':
    main(sys.argv)

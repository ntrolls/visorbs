import tidy
import sys
import os
import match
import shutil
import convert
import orbs
import sips
import glob
import util

if __name__ == "__main__":
	compiler = tidy.Tidy()

	source = sys.argv[1]
	source_path, source_name = os.path.split(source)
	source_name, source_ext = os.path.splitext(source_name)

	util.clean(source_name, sys.argv[2])
	target_img = util.prepare_target(source, sys.argv[2]) # use pre-converted-cropped bitmap 

	threshold = float(sys.argv[3]) if len(sys.argv) > 3 else 0.001
	
	criterion_name = os.path.split(target_img)[1]
	criterion_name = os.path.splitext(criterion_name)[0]

	# move source to the experiment directory
	harness_file = "harness_" + source_name + "_" + criterion_name + ".html"
	
	print "Using harness_file ", harness_file
	shutil.copyfile(source, harness_file)
	print "Copy successful"

	# slice!
	print "syntax check:", compiler.compile(harness_file)
	temp_png = util.rasterise_html(harness_file)
		
	lines = open(source, "r").readlines()
	
	tokens, pass_count, deleted, total = orbs.slice(lines, target_img, 
		threshold=threshold, 
		compiler=compiler, 
		build_file=harness_file, 
		temp_file=harness_file, 
		source_path=source_path, 
		source_name=source_name, 
		source_ext=source_ext, 
		criterion=criterion_name,
		convert=util.rasterise_html)

	# produce the result	
	print "end"
	temp_png = util.rasterise_html(harness_file)
	
	os.rename(temp_png, source.replace(".html", "_" + criterion_name + "_sliced.png"))
	os.rename(harness_file, source.replace(".html", "_" + criterion_name + "_sliced.html"))

	info_file = source.replace(".tex", "_" + criterion_name + "_stat.txt")
	info = open(info_file, "w")
	info.write("pass,deleted_lines,original_lines\n")
	info.write("%d,%d,%d\n" % (pass_count, deleted, total))
	info.close()

	for filename in glob.glob("harness_" + source_name + "_" + criterion_name + ".*"):
		os.remove(filename)



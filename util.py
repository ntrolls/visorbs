import cv
from PIL import Image
import numpy as np
import pdflatex, sys, os, match, shutil, convert, orbs, sips, glob
import subprocess
import sources

def prepare_target(source, target):
	source_name, source_ext = os.path.splitext(source)
	cropbox = sources.get_cropbox(source, target)
	return crop(source_name + "_original.png", cropbox, target)

def prepare_harness(harnessfile, tempfile):
	harness = open(harnessfile, "w")
	harness.write("""
		\\documentclass{standalone}
		\\usepackage{pagecolor}
		\\pagecolor{white}

		\\usepackage{tikz}
		\\usetikzlibrary{calc,decorations.markings,decorations.pathreplacing,calc,arrows,shapes,decorations}
		\\usetikzlibrary{decorations.pathmorphing}
		\\usepackage{nicefrac}
		\\usepackage{verbatim}

		\\newcommand{\\drawarcdelta}[4]{
		  \draw ($#1+(#2:#4)$) arc[start angle=#2, delta angle=#3, radius=#4];
		}

		\\newcommand{\\drawlabeledarcdelta}[6]{
		  \\drawarcdelta{#1}{#2}{#3}{#4}
		  \\node at ($#1+(#2+#3/2:#6)$) {#5};
		}

		\\begin{document}

		\\input{%s}

		\\end{document}
	""" % (tempfile))
	harness.close()
	return

def get_shape(imagefile):
	return np.asarray(Image.open(imagefile).convert("L")).shape

def build(subject):
	if subject.endswith(".tex"):
		return _build_latex(subject)
	elif subject.endswith("html"):
		return _build_html(subject)

def _build_html(source):
	compiler = pdflatex.PDFLatex()

	source_path, source_name = os.path.split(source)
	source_name, source_ext = os.path.splitext(source_name)

	# move source to the experiment directory
	harness_file = "harness_" + source_name + "_original.html"
	shutil.copyfile(source, harness_file)

	temp_png = rasterise_html(harness_file)
	print "temp_png:", temp_png
	print "source:", source
	built_file = source.replace(".html", "_original.png")
	os.rename(temp_png, built_file)

	for filename in glob.glob("harness_" + source_name + "_original.*"):
		os.remove(filename)
	return built_file

def _build_latex(source):
	compiler = pdflatex.PDFLatex()

	source_path, source_name = os.path.split(source)
	source_name, source_ext = os.path.splitext(source_name)

	# move source to the experiment directory
	temp_file = "temp_" + source_name + "_original.tex"
	shutil.copyfile(source, temp_file)

	# create harness for TikZ
	harness_file = "harness_" + source_name + "_original.tex"
	prepare_harness(harness_file, temp_file)

	# slice!
	ret, temp_pdf = compiler.compile(harness_file)
	ret, temp_png = sips.SIPS().convert(temp_pdf)
	print "temp_png:", temp_png
	print "source:", source
	built_file = source.replace(".tex", "_original.pdf")
	os.rename(temp_pdf, built_file)

	for filename in glob.glob("harness_" + source_name + "_original.*"):
		os.remove(filename)
	os.remove(temp_file)
	return built_file

def clean(subject, target):
	for filename in glob.glob("subjects/" + subject + "/*" + target + "*"):
		os.remove(filename)

def rasterise_html(input_file):
	input_name, input_ext = os.path.splitext(input_file)
	output_name = input_name + ".png"
	cmd = "phantomjs rasterise.js " + input_file + " " + output_name + " 800px"
	print "executing:", cmd
	process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
	out, error = process.communicate()
	exitcode = process.returncode
	print "exit code:", exitcode
	return output_name

def convert(inputfile):
	input_name, input_ext = os.path.splitext(inputfile)
	output_name = input_name + ".png"
	cmd = "convert -density 150 -units pixelsperinch"
	print "executing:", cmd + " " + inputfile + " " + output_name
	process = subprocess.Popen(cmd + " " + inputfile + " " + output_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
	out, error = process.communicate()
	exitcode = process.returncode
	print "exit code:", exitcode
	return output_name

def crop(inputfile, cropbox, output_name):
	path, input_filename = os.path.split(inputfile)
	input_name, input_ext = os.path.splitext(inputfile)
	output_file = os.path.join(path, output_name + ".png")
	(x, y, w, h) = cropbox
	cmd = "convert -crop %dx%d+%d+%d" % (w, h, x, y)
	print "Executing:", cmd + " " + inputfile + " " + output_file
	process = subprocess.Popen(cmd + " " + inputfile + " " + output_file, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
	out, error = process.communicate()
	exitcode = process.returncode
	print "exit code:", exitcode
	return output_file	

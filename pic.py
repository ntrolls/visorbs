import subprocess, tempfile, time, threading, sys, os, signal

class Pic(object):
	def __init__(self):
		# cat X | pic | eqn | groff -t -Tps -U | ps2pdf - >  pdg_figure.pdf
		self.cmd1 = "cat"
		self.cmd2 = "| pic | eqn | groff -t -Tps -U | ps2pdf - > "
		self.working_dir = None
		self.process = None
		self.out = None
		self.err = None
		self.exitcode = None
	
	def compile(self, inputfile):
		self.exitcode = 0

		wd = os.path.split(os.path.realpath(inputfile))[0]
		in_file = os.path.split(os.path.realpath(inputfile))[1]
		ps_file = os.path.realpath(inputfile).replace(".pic", ".ps")
		out_file = os.path.realpath(inputfile).replace(".pic", ".pdf")
		# print out_file
		
		of = open(ps_file, "w")
		p1 = subprocess.Popen(['groff', '-p', '-e', '-t', '-Tps', '-U', inputfile], stdout = of)
		of.close()
		p1.communicate()
		
		p2 = subprocess.Popen(['ps2pdf', ps_file, out_file], stdout = None)
		p2.communicate()
		
		returncode = p1.returncode | p2.returncode
		print p1.returncode, p2.returncode
		# os.remove(ps_file)

		return returncode, out_file

if __name__ == "__main__":
	compiler = Pic()
	exit, out = compiler.compile(sys.argv[1])
	print exit
	print "".join(out)
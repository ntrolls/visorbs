import subprocess, os, glob, multiprocessing, sys, util

subjects = [
	# "subjects/cone/cone.tex",
	# "subjects/hydrogen/hydrogen.tex",
	# "subjects/raindrop/raindrop.tex",
	# "subjects/shapes/shapes.tex",
	"subjects/threecols/threecols.html",
	# "subjects/shapes1/shapes_target1_sliced.tex",
	]
targets = [
	# "target1", 
	"target2", 
	"target3", 
	"target4", 
	"target5",
	]

def worker(cmd): 
	p = subprocess.Popen(cmd.split(" "));
	p.wait()

def clean():
	subjects = glob.glob("subjects/*")
	for subject in subjects:
		if os.path.isdir(subject):
			subject_path = subject
			subject_name = os.path.split(subject)[1]
			result_filter = "%s/%s_target*.*" % (subject_path, subject_name)
			for result_file in glob.glob(result_filter):
				os.remove(result_file)

def main(num_proc):
	clean()
	cmd_list = []
	for subject in subjects:
		built_file = util.build(subject)
		built_image = util.convert(built_file)	
		for target in targets:	
			if subject.endswith("tex"):
				cmd_list.append("python knife_tikz.py %s %s" % (subject, target))
			elif subject.endswith("pic"):
				cmd_list.append("python knife_pic.py %s %s" % (subject, target))
			elif subject.endswith("html"):
				cmd_list.append("python knife_html.py %s %s" % (subject, target))
	print cmd_list
	pool = multiprocessing.Pool(processes = num_proc)
	results =[pool.apply_async(worker, [cmd]) for cmd in cmd_list]
	ans = [res.get() for res in results]

if __name__ == '__main__':
	num_proc = int(sys.argv[1])
	main(num_proc)

import subprocess, tempfile, time, threading, sys, os, signal

class PDFLatex(object):
	def __init__(self):
		self.cmd = "pdflatex -interaction=nonstopmode"
		self.working_dir = None
		self.process = None
		self.out = None
		self.err = None
		self.exitcode = None
	
	def compile(self, inputfile):
		output_file = inputfile.replace(".tex", ".pdf")
		def target():
			self.process = subprocess.Popen(self.cmd + " " + inputfile, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
			self.out, self.error = self.process.communicate()
			self.exitcode = self.process.returncode
		thread = threading.Thread(target=target)
		thread.start()
		thread.join(5)
		if thread.is_alive():
			os.killpg(self.process.pid, signal.SIGTERM)
			self.process.kill()
			print "timeout!"
			# print self.out
			return -9, None
		# print self.out
		return self.exitcode, output_file

if __name__ == "__main__":
	compiler = PDFLatex()
	exit, out = compiler.compile(sys.argv[1])
	print out
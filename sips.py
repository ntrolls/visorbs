import subprocess, tempfile, time, threading, sys, os, signal

class SIPS(object):
	def __init__(self):
		self.cmd = "sips -s format png"
		self.output_name = ""
		self.working_dir = None
		self.process = None
		self.out = None
		self.err = None
		self.exitcode = None
	
	def convert(self, inp):
		def target():
			input_name, input_ext = os.path.splitext(inp)
			self.output_name = input_name + ".png"
			print inp, "->", self.output_name
			self.process = subprocess.Popen(self.cmd + " " + inp + " --out " + self.output_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid)
			self.out, self.error = self.process.communicate()
			self.exitcode = self.process.returncode
		thread = threading.Thread(target=target)
		thread.start()
		thread.join(10)
		if thread.is_alive():
			os.killpg(self.process.pid, signal.SIGTERM)
			self.process.kill()
			return -9, []
		# return self.exitcode, self.out.split("\n")
		return self.exitcode, self.output_name

if __name__ == '__main__':
	sips = SIPS()
	print sips.convert(sys.argv[1])